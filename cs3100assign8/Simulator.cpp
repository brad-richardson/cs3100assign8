#include "Simulator.hpp"

Simulator::Simulator(int numCPU, int numIO, double endTime,
	double contextSwitchCost, double freq, double cpuBoundPct, std::string algorithm, 
	bool fifoMemory, int numCPUPages, double pageFaultCost)
	: numCPU(numCPU), numIO(numIO), endTime(endTime),
	contextSwitchCost(contextSwitchCost), freq(freq), 
	cpuBoundPct(cpuBoundPct), algorithm(algorithm), 
	fifoMemory(fifoMemory), numCPUPages(numCPUPages), pageFaultCost(pageFaultCost)
{
	timeToNextSpawn = freq;
	taskDist = std::uniform_int_distribution<int>(minTasks, maxTasks);
	if (cpuBoundPct > 1 || cpuBoundPct < 0)
	{
		std::cout << "Invalid CPU bound pct, using 0.5..." << std::endl;
		cpuBoundPct = 0.5;
	}
	cpuBoundDist = std::bernoulli_distribution(cpuBoundPct);
	waitSet.resize(numIO);
	cpuCache.resize(numCPUPages);
	loaded = true;
}

double Simulator::getMemory(std::shared_ptr<Process> p)
{
	int neededPage = p.get()->getPage();
	for (Page pg : cpuCache)
	{
		if (pg.getPageNumber() == neededPage)
		{
			return 0;
		}
	}
	//doesn't exist in cache, pop off first page and add new one
	cpuCache.pop_front();
	cpuCache.push_back(Page(neededPage));
	pageFaults++;
	return pageFaultCost;
}

std::string Simulator::printStats()
{
	std::stringstream ss;
	ss << std::endl << "Stats: " << std::endl;
	ss << "Processes finished/created: " << doneSet.size() << "/" << processesSpawned << std::endl;
	std::vector<double> responseVals;
	for (std::shared_ptr<Process> p : readySet)
	{
		responseVals.push_back(p.get()->getResponse());
	}
	for (std::shared_ptr<Process> p : executeSet)
	{
		responseVals.push_back(p.get()->getResponse());
	}
	for (std::deque<std::shared_ptr<Process>> &set : waitSet)
	{
		for (std::shared_ptr<Process> p : set)
		{
			responseVals.push_back(p.get()->getResponse());
		}
	}
	for (std::shared_ptr<Process> p : doneSet)
	{
		responseVals.push_back(p.get()->getResponse());
	}
	if ((int) responseVals.size() > 0 && mean(responseVals)> 0)
	{
		ss << "Response - avg: " << mean(responseVals)
			<< " min: " << findMin(responseVals) << " max: " << findMax(responseVals)
			<< " sd: " << stdDev(responseVals) << std::endl;
	}
	else
	{
		ss << "Response - no processes completed any cycles" << std::endl;
	}
	std::vector<double> latencyVals;
	for (std::shared_ptr<Process> p : doneSet)
	{
		latencyVals.push_back(p.get()->getLatency());
	}
	if (latencyVals.size() > 0)
	{
		ss << "Latency - avg: " << mean(latencyVals)
			<< " min: " << findMin(latencyVals) << " max: " << findMax(latencyVals)
			<< " sd: " << stdDev(latencyVals) << std::endl;
	}
	else
	{
		ss << "Latency - no processes finished" << std::endl;
	}

	ss << "CPU utilization - avg: " << mean(utilization) << " min: 0" << " max: 1" << " sd: " << stdDev(utilization) << std::endl;
	ss << "Processes still in ready set: " << readySet.size() << std::endl;
	ss << "Processes still in execute set: " << executeSet.size() << std::endl;
	int processesInWaitSet = 0;
	for (std::deque<std::shared_ptr<Process>> &set : waitSet)
	{
		for (std::shared_ptr<Process> p : set)
		{
			processesInWaitSet++;
		}
	}
	ss << "Processes still in wait set: " << processesInWaitSet << std::endl;
	ss << "Throughput: " << doneSet.size() * 100 / endTime << " processes/100 time" << std::endl;
	ss << "Page Faults: " << pageFaults << std::endl << std::endl;

	return ss.str();
}

std::string Simulator::toString()
{
	std::stringstream ss;
	ss << "Simulator settings: " << std::endl;
	ss << "Algorithm: " << algorithm << std::endl;
	ss << "CPUs: " << numCPU << std::endl;
	ss << "IO devices: " << numIO << std::endl;
	ss << "End time: " << endTime << std::endl;
	ss << "Context switch cost: " << contextSwitchCost << std::endl;
	ss << "Process generation frequency: " << freq << std::endl;
	ss << "CPU bound task ratio: " << cpuBoundPct << std::endl;
	ss << "Page size: " << numCPUPages << std::endl;
	ss << "Page fault cost: " << pageFaultCost << std::endl;
	ss << printStats();
	return ss.str();
}