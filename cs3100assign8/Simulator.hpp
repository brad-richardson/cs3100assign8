#ifndef SIMULATOR_HPP
#define SIMULATOR_HPP

#include <iostream>
#include <deque>
#include <vector>
#include <string>
#include <sstream>
#include <random>
#include <algorithm>
#include "process.hpp"
#include "tools.hpp"
#include "Page.hpp"

//Base class
class Simulator {
protected:
	std::string algorithm;
	int numCPU;
	int numIO;
	int processesSpawned = 0;
	bool fifoMemory;
	int numCPUPages;
	//determines how many tasks(or bursts) per process
	int minTasks = 2;
	int maxTasks = 10;

	double currTime = 0;
	double endTime;

	double contextSwitchCost;
	double pageFaultCost;
	double freq;
	double timeToNextSpawn;
	double cpuBoundPct;

	std::vector<double> utilization;
	int pageFaults = 0;

	std::default_random_engine generator;
	std::uniform_int_distribution<int> taskDist;
	std::bernoulli_distribution cpuBoundDist;

	std::deque<std::shared_ptr<Process>> readySet;
	std::vector<std::shared_ptr<Process>> executeSet;
	std::vector<std::deque<std::shared_ptr<Process>>> waitSet;
	std::vector<std::shared_ptr<Process>> doneSet;

	std::deque<Page> cpuCache;
	bool loaded = false;

public:
	Simulator(int, int, double,
		double, double, double, std::string, 
		bool, int, double);

	virtual void runSim()=0;

	virtual double findNextEvent()=0;
	//"loads" memory into cpu cache and returns the cost (if any) to load it
	double getMemory(std::shared_ptr<Process>);

	std::string printStats();

	std::string toString();

};

#endif