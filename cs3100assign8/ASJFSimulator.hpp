#ifndef ASJFSIMULATOR_HPP
#define ASJFSIMULATOR_HPP

#include "Simulator.hpp"


//Round robin simulator for tasks scheduling
class ASJFSimulator : public Simulator {
private:
	//idea from http://stackoverflow.com/questions/2439283/how-can-i-create-min-stl-priority-queue
	struct compare
	{
		bool operator()(const std::shared_ptr<Process> lhs, const std::shared_ptr<Process> rhs)
		{
			return lhs.get()->avgCPUTime() > rhs.get()->avgCPUTime();
		}
	};
	std::priority_queue<std::shared_ptr<Process>, std::vector<std::shared_ptr<Process>>, compare> readyPQ;

public:
	ASJFSimulator(int numCPU, int numIO, double endTime,
		double contextSwitchCost, double freq, double cpuBoundPct,
		int numCPUPages, double pageFaultCost);

	void runSim();

	double findNextEvent();
};

#endif