
class Page {

private:
	int number;
	bool secondChance;
	double timeAdded;
	double lastUse;

public:
	Page(int number = -1)
		: number(number)
	{
		secondChance = false;
	}

	int getPageNumber()
	{
		return number;
	}

	bool hasSecondChance()
	{
		if (secondChance)
		{
			secondChance = false;
			return true;
		}
		else
		{
			return false;
		}
	}

};