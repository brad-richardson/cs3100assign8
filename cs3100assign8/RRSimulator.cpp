#include "RRSimulator.hpp"

RRSimulator::RRSimulator(int numCPU, int numIO, double endTime, 
		double contextSwitchCost, double freq, double cpuBoundPct,
		int numCPUPages, double pageFaultCost, double quantumSize)
		: Simulator(numCPU, numIO, endTime,
		contextSwitchCost, freq, cpuBoundPct, "Round Robin",
		true, numCPUPages, pageFaultCost),
		quantumSize(quantumSize)
{
	runSim();
}

void RRSimulator::runSim()
{
	while(currTime < endTime)
	{
		double nextEventTime = findNextEvent();

		//change all values as if time has passed
		currTime += nextEventTime;
		timeToNextSpawn -= nextEventTime;
		for(std::shared_ptr<Process> p : executeSet)
		{
			p.get()->subtractCPUTime(nextEventTime);
		}
		for (double &time : timeLeftInCPU)
		{
			time -= nextEventTime;
		}
		for (std::deque<std::shared_ptr<Process>> &set : waitSet)
		{
			if (set.size() > 0)
				set.at(0).get()->subtractIOTime(nextEventTime);
		}
		

		//check for new process to spawn and put on ready queue
		if(timeToNextSpawn <= 0)
		{
			processesSpawned++;
			readySet.push_back(
				std::make_shared<Process>(taskDist(generator), cpuBoundDist(generator), numIO, currTime, numCPUPages));
			timeToNextSpawn = freq; //reset timer
		}
		//measures current cpu usage
		utilization.push_back(executeSet.size() / numCPU);
		//check for open cpu
		//if open cpu, add cpu time and move to cpu queue
		if((int)executeSet.size() < numCPU && !readySet.empty())
		{
			std::shared_ptr<Process> p = readySet.front();
			p->addCPUTime(contextSwitchCost);
			executeSet.push_back(p);
			timeLeftInCPU.push_back(quantumSize);
			p->addCPUTime(getMemory(p));
			readySet.pop_front();
		}

		//if cpu process is done, move to io queue
		for(int i = 0; i < (int)executeSet.size(); ++i)
		{
			std::shared_ptr<Process> p = executeSet[i];
			//time left for actual process
			double timeLeft = p->getCurrCPU();
			if(timeLeft <= 0)
			{
				waitSet.at(p->getCurrIODevice()).push_back(p);
				std::swap(executeSet[i], executeSet.back());
				executeSet.pop_back();
				std::swap(timeLeftInCPU[i], timeLeftInCPU.back());
				timeLeftInCPU.pop_back();
			}
			//granted time left for round robin
			else if (timeLeftInCPU[i] <= 0)
			{
				readySet.push_back(p);
				std::swap(executeSet[i], executeSet.back());
				executeSet.pop_back();
				std::swap(timeLeftInCPU[i], timeLeftInCPU.back());
				timeLeftInCPU.pop_back();
			}
		}

		//if io is done then cycle process
		//if process is done then move to finish set
		//otherwise move back to ready set
		for(int i = 0; i < (int)waitSet.size(); ++i)
		{
			//only process we are interested in is the first one in each io device queue
			if (waitSet.at(i).size() > 0)
			{
				std::shared_ptr<Process> p = waitSet.at(i).at(0);
				double timeLeft = p->getCurrIO();
				if (timeLeft <= 0)
				{
					p->doCycle(currTime);
					if (p->isDone())
					{
						doneSet.push_back(p);
					}
					else
					{
						readySet.push_back(p);
					}
					waitSet.at(i).pop_front();
				}
			}
		}
	}
	return;
}

//finds next(smallest) event to "run"
double RRSimulator::findNextEvent()
{
	// TODO this now needs to also check timeLeftInCPU as a posible next event

	//finds smallest of all events in cpu, io and next spawn time
	double nextRoundRobin, shortestCPU, shortestIO;
	nextRoundRobin = shortestCPU = shortestIO = std::numeric_limits<double>::max();
	//finds smallest and compares all three
	for(std::shared_ptr<Process> p : executeSet)
	{
		double newVal = p.get()->getCurrCPU();
		if(newVal < shortestCPU)
		{
			shortestCPU = newVal;
		}
	}
	for (double time : timeLeftInCPU)
	{
		if (time < nextRoundRobin)
		{
			nextRoundRobin = time;
		}
	}
	for(int i = 0; i < (int)waitSet.size() && waitSet.at(i).size() > 0; ++i)
	{
		double newVal = waitSet.at(i).at(0).get()->getCurrIO();
		if(newVal < shortestIO)
		{
			shortestIO = newVal;
		}
	}
	return std::min(nextRoundRobin, std::min(shortestCPU, std::min(shortestIO, timeToNextSpawn)));
}