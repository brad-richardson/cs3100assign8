#ifndef RRSIMULATOR_HPP
#define RRSIMULATOR_HPP

#include "Simulator.hpp"

//Round robin simulator for tasks scheduling
class RRSimulator : public Simulator {
private:
	double quantumSize;
	std::vector<double> timeLeftInCPU;

public:
	RRSimulator(int numCPU, int numIO, double endTime, 
		double contextSwitchCost, double freq, double cpuBoundPct, 
		int numCPUPages, double pageFaultCost, double quantumSize);

	void runSim();

	double findNextEvent();

};

#endif