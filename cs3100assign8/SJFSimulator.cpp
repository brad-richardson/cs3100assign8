#include "SJFSimulator.hpp"

SJFSimulator::SJFSimulator(int numCPU, int numIO, double endTime,
	double contextSwitchCost, double freq, double cpuBoundPct,
	int numCPUPages, double pageFaultCost)
	: Simulator(numCPU, numIO, endTime,
	contextSwitchCost, freq, cpuBoundPct, "Shortest Job First",
	true, numCPUPages, pageFaultCost)
{
	runSim();
}

void SJFSimulator::runSim()
{
	while (currTime < endTime)
	{
		double nextEventTime = findNextEvent();

		//change all values as if time has passed
		currTime += nextEventTime;
		timeToNextSpawn -= nextEventTime;
		for (std::shared_ptr<Process> &p : executeSet)
		{
			p.get()->subtractCPUTime(nextEventTime);
		}
		for (std::deque<std::shared_ptr<Process>> &set : waitSet)
		{
			if (set.size() > 0)
				set.at(0).get()->subtractIOTime(nextEventTime);
		}

		//check for new process to spawn and put on ready queue
		if (timeToNextSpawn <= 0)
		{
			processesSpawned++;
			readyPQ.push(
				std::make_shared<Process>(taskDist(generator), cpuBoundDist(generator), numIO, currTime, numCPUPages));
			timeToNextSpawn = freq; //reset timer
		}
		//measures current cpu usage
		utilization.push_back(executeSet.size() / numCPU);
		//check for open cpu
		//if open cpu, add cpu time and move to cpu queue
		if ((int) executeSet.size() < numCPU && !readyPQ.empty())
		{
			//change this to findShortestNextJob() and push that
			std::shared_ptr<Process> p = readyPQ.top();
			readyPQ.pop();
			p.get()->addCPUTime(contextSwitchCost);
			p->addCPUTime(getMemory(p));
			executeSet.push_back(p);
		}

		//if cpu process is done, move to io queue
		for (int i = 0; i < (int) executeSet.size(); ++i)
		{
			std::shared_ptr<Process> p = executeSet[i];
			double timeLeft = p.get()->getCurrCPU();
			if (timeLeft <= 0)
			{
				waitSet.at(p.get()->getCurrIODevice()).push_back(p);
				std::swap(executeSet[i], executeSet.back());
				executeSet.pop_back();
			}
		}

		//if io is done then cycle process
		//if process is done then move to finish set
		//otherwise move back to ready set
		for (int i = 0; i < (int) waitSet.size(); ++i)
		{
			//only process we are interested in is the first one in each io device queue
			if (waitSet.at(i).size() > 0)
			{
				std::shared_ptr<Process> p = waitSet.at(i).at(0);
				double timeLeft = p.get()->getCurrIO();
				if (timeLeft <= 0)
				{
					p.get()->doCycle(currTime);
					if (p.get()->isDone())
					{
						doneSet.push_back(p);
					}
					else
					{
						readyPQ.push(p);
					}
					waitSet.at(i).pop_front();
				}
			}
		}
	}
	return;
}

//finds next(smallest) event to "run"
double SJFSimulator::findNextEvent()
{
	//finds smallest of all events in cpu, io and next spawn time
	double shortestCPU, shortestIO;
	shortestCPU = shortestIO = std::numeric_limits<double>::max();
	//finds smallest and compares all three
	for (std::shared_ptr<Process> p : executeSet)
	{
		double newVal = p.get()->getCurrCPU();
		if (newVal < shortestCPU)
		{
			shortestCPU = newVal;
		}
	}
	for (int i = 0; i < (int) waitSet.size() && waitSet.at(i).size() > 0; ++i)
	{
		double newVal = waitSet.at(i).at(0).get()->getCurrIO();
		if (newVal < shortestIO)
		{
			shortestIO = newVal;
		}
	}
	return std::min(shortestCPU, std::min(shortestIO, timeToNextSpawn));
}
