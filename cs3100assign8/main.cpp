#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include "FIFOSimulator.hpp"
#include "RRSimulator.hpp"
#include "SJFSimulator.hpp"
#include "ASJFSimulator.hpp"

int main(int argc, char* argv[])
{
	bool runMemPageTest = false;
	int cpu, ioDevices, numCPUPages;
	double endTime, contextSwitchCost, freq, cpuBoundPct, quantumSize, pageFaultCost;

	if (argc < 10)
	{
		char choice;
		std::cout << "Use default values or test page faults (y/n/t)? ";
		std::cin >> choice;
		if (choice == 'y')
		{
			std::cout << "Using default values to run simulations..." << std::endl;
			cpu = 8;
			ioDevices = 16;
			endTime = 10000;
			contextSwitchCost = .2;
			freq = 5;
			cpuBoundPct = 0.50;
			numCPUPages = 200;
			pageFaultCost = .4;
			quantumSize = 5;
		}
		else if (choice == 't')
		{
			std::cout << "Running simulation using defaults and using different memory sizes..." << std::endl;
			cpu = 8;
			ioDevices = 16;
			endTime = 10000;
			contextSwitchCost = .2;
			freq = 5;
			cpuBoundPct = 0.50;
			//numCPUPages = 0;
			runMemPageTest = true;
			pageFaultCost = .4;
			quantumSize = 5;
		}
		else
		{
			//allows for input from a file
			std::cout << "How many processors? ";
			std::cin >> cpu;
			std::cout << "How many IO devices? ";
			std::cin >> ioDevices;
			std::cout << "End time? ";
			std::cin >> endTime;
			std::cout << "Context switch cost? ";
			std::cin >> contextSwitchCost;
			std::cout << "Process creation frequency? ";
			std::cin >> freq;
			std::cout << "CPU bound task ratio? ";
			std::cin >> cpuBoundPct;
			std::cout << "Number of CPU Pages? ";
			std::cin >> numCPUPages;
			std::cout << "Page fault cost? ";
			std::cin >> pageFaultCost;
			std::cout << "Quantum size (Round Robin): ";
			std::cin >> quantumSize;
		}
	}
	else if (argc == 10)
	{
		cpu = atoi(argv[1]);
		ioDevices = atoi(argv[2]);
		endTime = atoi(argv[3]);
		contextSwitchCost = atof(argv[4]);
		freq = atof(argv[5]);
		cpuBoundPct = atof(argv[6]);
		numCPUPages = atoi(argv[7]);
		pageFaultCost = atof(argv[8]);
		quantumSize = atof(argv[9]);
	}

	if (runMemPageTest == false)
	{
		FIFOSimulator fifosim(cpu, ioDevices, endTime, contextSwitchCost, freq, cpuBoundPct, numCPUPages, pageFaultCost);
		std::cout << fifosim.toString();

		RRSimulator rrsim(cpu, ioDevices, endTime, contextSwitchCost, freq, cpuBoundPct, numCPUPages, pageFaultCost, quantumSize);
		std::cout << rrsim.toString();

		SJFSimulator sjfsim(cpu, ioDevices, endTime, contextSwitchCost, freq, cpuBoundPct, numCPUPages, pageFaultCost);
		std::cout << sjfsim.toString();

		ASJFSimulator asjfsim(cpu, ioDevices, endTime, contextSwitchCost, freq, cpuBoundPct, numCPUPages, pageFaultCost);
		std::cout << asjfsim.toString();
	}
	else
	{
		int MAXPAGESIZE = 1000;//see process.cpp
		std::ofstream fout("pagefaultoutput.txt");
		for (int i = 1; i < MAXPAGESIZE; i++)
		{
			FIFOSimulator fifosim(cpu, ioDevices, endTime, contextSwitchCost, freq, cpuBoundPct, i, pageFaultCost);
			fout << fifosim.toString();

			RRSimulator rrsim(cpu, ioDevices, endTime, contextSwitchCost, freq, cpuBoundPct, i, pageFaultCost, quantumSize);
			fout << rrsim.toString();

			SJFSimulator sjfsim(cpu, ioDevices, endTime, contextSwitchCost, freq, cpuBoundPct, i, pageFaultCost);
			fout << sjfsim.toString();

			ASJFSimulator asjfsim(cpu, ioDevices, endTime, contextSwitchCost, freq, cpuBoundPct, i, pageFaultCost);
			fout << asjfsim.toString();
		}
		fout.close();
	}
	
	return 0;
}