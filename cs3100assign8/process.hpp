#ifndef PROCESS_HPP
#define PROCESS_HPP

#include <queue>
#include <string>
#include <sstream>
#include <memory>
#include <random>
#include "tools.hpp"

typedef std::pair<double, double> Task;

class Process
{
private:
	std::deque<Task> tasks;
	std::deque<int> ioDeviceToUse;
	std::vector<double> prevCPUTimes;
	double prevCPUTimeAvg;

	bool cpuBound;
	int ioDevices;
	double initTime = -1;
	double response = -1; //time between init and first cycle completion
	double latency = -1; //time between creation and completion	
	int numCPUPages;
	int memoryPageNeeded = -1;

public:
	Process(int, bool, int, double, int);
	
	Task getTask();
	//for ASJF, keep track of average cpu time
	void storeCPUTime();
	double avgCPUTime();
	void subtractCPUTime(double time);
	void addCPUTime(double time);
	void subtractIOTime(double time);
	double getCurrCPU();
	double getCurrIO();
	int getCurrIODevice();
	double getResponse();
	double getLatency();
	int getPage();
	bool isCPUBound();
	bool isDone();

	void doCycle(double currTime);

	std::string toString();

};

#endif